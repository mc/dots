#!/bin/bash
 
res=$(echo "lock|logout|reboot|shutdown" | rofi -sep "|" -dmenu -i -p 'Power Menu' "" -width 12 -hide-scrollbar -location 3 -yoffset 30 -padding 12 -opacity 100 -no-fullscreen) 
 
if [ $res = "lock" ]; then
    $HOME/.config/i3/lock.sh
fi
if [ $res = "logout" ]; then
    i3-msg exit
fi
if [ $res = "reboot" ]; then
    systemctl reboot
fi
if [ $res = "shutdown" ]; then
    systemctl poweroff
fi
exit 0
