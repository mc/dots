#!/bin/bash

WIDTH=500
HEIGHT=500
screen_dims=$(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/')
screen_dims=(${screen_dims//x/ })
echo "screen: ${screen_dims[0]} x ${screen_dims[1]}"

center_x=$(echo "${screen_dims[0]} / 2 - $WIDTH" | bc)
center_y=$(echo "${screen_dims[1]} / 2 - $HEIGHT" | bc)

function urlencode() {
    echo -n "$1" | perl -MURI::Escape -ne 'print uri_escape($_)'
}
 
query=$(echo "" | rofi -dmenu -p "rustdoc" -lines 0 -width 20)

[ -z "$query" ] && echo "no query." && exit 1

base=""
search=""
if [[ "$query" =~ ^.+\/.+$ ]]; then
    pieces=(${query//\// })
    crate=${pieces[0]}
    search=${pieces[1]}
    echo "querying docs.rs/$crate for \"$search\"..."
    full_url=$(curl -sI "https://docs.rs/$crate" | grep -i "location: " | cut -c11-)
    full_url=${full_url//[[:space:]]}
    echo "resolved to $full_url"
    base="${full_url}"
else
    toolchain=$(dirname $(dirname $(rustup which rustc)))
    docpath="$toolchain/share/doc/rust/html/std/index.html"
    base="file://$docpath"
    search="$query"
fi

path="${base}?search=$(urlencode "$search")"

echo "opening $path"
chromium --new-window --app="$path" --enable-extensions --window-size="${WIDTH},${HEIGHT}" --window-position="${center_x},${center_y}"

