#!/bin/bash

#SET="xbacklight -set"
#INC="xbacklight -inc"
#DEC="xbacklight -inc"
GET="light -G"
SET="light -S"
INC="light -A"
DEC="light -U"

current=$($GET)
case "$1" in
    up)
        if (( $(echo "$current <= 0.05" | bc -l) )); then
            $SET 0.1
        elif (( $(echo "$current < 5" | bc -l) )); then
            $SET 5
        elif (( $(echo "$current < 47.5" | bc -l) )); then
            $INC 5
        else
            $INC 10
        fi
        ;;
    down)
        if (( $(echo "$current <= 0.1" | bc -l) )); then
            $SET 0
        elif (( $(echo "$current <= 5.1" | bc -l) )); then
            $SET 0.1
        elif (( $(echo "$current <= 50.1" | bc -l) )); then
            $DEC 5
        else
            $DEC 10
        fi
        ;;
esac
