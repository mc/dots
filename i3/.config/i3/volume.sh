#!/bin/bash

default_sink=$(pacmd list-sinks | sed -rn '/\* index: /!d; { s/.*index: (.*)/\1/p }')
default_source=$(pacmd list-sources | sed -rn '/\* index: /!d; { s/.*index: (.*)/\1/p }')

echo "sink $default_sink source $default_source"

case "$1" in
    up) pactl set-sink-volume @DEFAULT_SINK@ +5% ;;
    down) pactl set-sink-volume $default_sink -5% ;;
    mute) pactl set-sink-mute $default_sink toggle ;;
    mute-mic) pactl set-source-mute $default_source toggle ;;
esac

