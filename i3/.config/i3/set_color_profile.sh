#!/bin/bash

colormgr device-add-profile \
    /org/freedesktop/ColorManager/devices/xrandr_eDP1_ichi_1000 \
    /org/freedesktop/ColorManager/profiles/icc_c51ef5881cf5ea5639f15f07a0fceb81_ichi_1000

colormgr device-make-profile-default \
  /org/freedesktop/ColorManager/devices/xrandr_eDP1_ichi_1000 \
  /org/freedesktop/ColorManager/profiles/icc_c51ef5881cf5ea5639f15f07a0fceb81_ichi_1000
