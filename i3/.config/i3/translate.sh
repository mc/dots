#!/bin/bash

translated=$(trans -show-original n \
        -show-alternatives n \
        -source ja \
        -target en \
        "$(xclip -o -selection)")

echo "$translated"

echo "press enter to copy to clipboard or ctrl-c to quit."
IFS=
read -n 1 key

echo "$translated" | xclip -selection c

echo "copied to clipboard."

