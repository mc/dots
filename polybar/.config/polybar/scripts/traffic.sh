#!/bin/bash

function human_readable {
        VALUE=$(($1/1000))
        BIGGIFIERS=( K M G )
        CURRENT_BIGGIFIER=0
        while [ $VALUE -gt 2000 ] ;do
                VALUE=$(($VALUE/1000))
                CURRENT_BIGGIFIER=$((CURRENT_BIGGIFIER+1))
        done
        echo "$VALUE ${BIGGIFIERS[$CURRENT_BIGGIFIER]}/s"
}

DEVICE=$(ip route get 8.8.8.8 | grep -ohP '(?<=dev )[\w-]*')
IS_GOOD=0
for GOOD_DEVICE in `grep ":" /proc/net/dev | awk '{print $1}' | sed s/:.*//`; do
        if [ "$DEVICE" = "$GOOD_DEVICE" ]; then
                IS_GOOD=1
                break
        fi
done

if [ $IS_GOOD -eq 0 ]; then
        echo "Device $DEVICE not found. Should be one of these:"
        grep ":" /proc/net/dev | awk '{print $1}' | sed s/:.*//
        exit 1
fi

LINE=`grep $DEVICE /proc/net/dev | sed s/.*://`;
RECEIVED=`echo $LINE | awk '{print $1}'`
TRANSMITTED=`echo $LINE | awk '{print $9}'`
TOTAL=$(($RECEIVED+$TRANSMITTED))

SLP=1
sleep $SLP

LINE=`grep $DEVICE /proc/net/dev | sed s/.*://`;
NEW_RECEIVED=`echo $LINE | awk '{print $1}'`
NEW_TRANSMITTED=`echo $LINE | awk '{print $9}'`
DOWN_SPEED=$((($NEW_RECEIVED-$RECEIVED)/$SLP))
UP_SPEED=$((($NEW_TRANSMITTED-$TRANSMITTED)/$SLP))

case $1 in
        up)
                echo $(human_readable $UP_SPEED)
                ;;
        down)
                echo $(human_readable $DOWN_SPEED)
                ;;
        *)
                echo "UP: $(human_readable $UP_SPEED)"
                echo "DOWN: $(human_readable $DOWN_SPEED)"
                ;;
esac
