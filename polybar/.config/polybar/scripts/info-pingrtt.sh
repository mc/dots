#!/bin/bash

HOST=8.8.8.8
TIMEOUT=1500
COUNT=3
SYMBOL="#"

fping_out=$(fping -c $COUNT -t $TIMEOUT -p $TIMEOUT $HOST 2>&1)
if [[ "$?" -ne "0" ]]; then
	echo "%{F#d60606}no ping%{F-}"
	exit 0
fi

raw=$(echo "$fping_out" | sed -rn 's%.*min/avg/max = [^/]*/([^/]*)/.*%\1%p')

rtt=$(printf "%.0f" $raw) 

if [[ "$rtt" -lt "50" ]]; then
    icon="%{F#3cb703}$SYMBOL%{F-}"
elif [[ "$rtt" -lt "150" ]]; then
    icon="%{F#f9dd04}$SYMBOL%{F-}"
else
    icon="%{F#d60606}$SYMBOL%{F-}"
fi

#echo "$icon $rtt ms"
echo "$rtt ms"

