#!/bin/bash

# get the name of the interface that handles normal traffic
interface=$(ip route get 8.8.8.8 | sed -rn 's/.* dev ([[:graph:]]*) .*/\1/p')

if ip link show type wireguard | grep -q "$interface"; then
    echo "$interface"
else
    echo "%{F#33F79256}wg%{F-}"
fi

