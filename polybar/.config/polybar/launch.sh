#!/usr/bin/env sh

killall -q polybar

while pgrep -x polybar >/dev/null; do sleep 1; done

export LANG="ja_JP.UTF-8"
export LC_ALL="ja_JP.UTF-8"

for m in $(polybar -m | sed -e 's/:.*$//g'); do
  env MONITOR="$m" nohup polybar bar &
  env MONITOR="$m" nohup polybar netspeedtop &
  env MONITOR="$m" nohup polybar netspeedbottom &
done

# notify-send 'polybars reloaded'

echo "Bars launched..."

