set -x PATH $HOME/bin $HOME/.cargo/bin $HOME/go/bin $PATH /opt/depot_tools $HOME/.gem/ruby/2.5.0/bin $HOME/git/tonari/portal/scripts
set -x TERMINAL "kitty"
set -x EDITOR "nvim"
set -x FFF_FAV1 "~/movies"
set -x FFF_FAV2 "~/music"
set -x FFF_FAV3 "~/git"
set -x CARGO_BUILD_PIPELINING true
set -x RUSTFLAGS "-C link-arg=-fuse-ld=lld"
set -x VAULT_ADDR "http://10.70.0.1:8200"
set -x AIRCON_TOPIC "ec555c89-3826-494f-bcbe-64cec22d143c" 

bind \cf 'forward-bigword'
bind \cb 'backward-bigword'

# eval (python -m virtualfish)
# vf activate default

function fish_greeting
  echo ""
  # kitty +kitten icat --place 50x20@3x1 --scale-up (shuf -n1 -e ~/.local/share/termicons/*)
  # echo -e "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
end

function myip
  dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | awk -F'\"' '{ print $2}'
end

if status --is-interactive
  alias df='df -h'
  alias ls='exa -F'
  alias lsl='exa -lF'
  alias ll='exa -alhF -snew --group-directories-first'
  alias lf="du -sh {.,}* | sort -h"
  alias lsnow="ll *(mm-5)"
  alias lstoday="ll *(mh-24)"
  alias x="exa --group-directories-first"
  alias zr="source $HOME/.zshrc"
  #  alias R="rsync -shavP"
  alias porn=' mpv "http://www.pornhub.com/random"'
  alias cls="echo -e '\0033\0143'"
  alias lock='$HOME/.config/i3/lock.sh'
  alias prettyping='prettyping --nolegend'
  alias ytdl='youtube-dl --output ~/Videos/'
  alias ytdlmq='youtube-dl --output ~/Videos/ -f "bestvideo[height<=720]+bestaudio/best[height<=720]"'

  alias open='xdg-open'
  alias :q='exit'
  alias dud='du -d 1 -h'
  alias duf='du -sh *'
  alias p='ps axo pid,user,pcpu,comm'
  alias mp3-dl='youtube-dl --ignore-config --extract-audio \
      --audio-format "mp3" --audio-quality 0 --embed-thumbnail \
      --add-metadata --metadata-from-title "%(artist)s - %(title)s" \
      --output "%(title)s.%(ext)s"'

  alias caffeinate='systemd-inhibit --what=handle-lid-switch --why="caffeinated"'

  set _git_log_medium_format '%C(bold)Commit:%C(reset) %C(green)%H%C(red)%d%n%C(bold)Author:%C(reset) %C(cyan)%an <%ae>%n%C(bold)Date:%C(reset)   %C(blue)%ai (%ar)%C(reset)%n%+B'
  set _git_log_oneline_format '%C(green)%h%C(reset) %s%C(red)%d%C(reset)%n'
  set _git_log_brief_format '%C(green)%h%C(reset) %s%n%C(blue)(%ar by %an)%C(red)%d%C(reset)%n'

  set _git_status_ignore_submodules 'none'

  # Git
  # alias g='git'

  # Branch (b)
  # alias gb='git branch'
  # alias gba='git branch --all --verbose'
  # alias gbc='git checkout -b'
  # alias gbd='git branch --delete'
  # alias gbD='git branch --delete --force'
  # alias gbl='git branch --verbose'
  # alias gbL='git branch --all --verbose'
  # alias gbm='git branch --move'
  # alias gbM='git branch --move --force'
  # alias gbr='git branch --move'
  # alias gbR='git branch --move --force'
  # alias gbs='git show-branch'
  # alias gbS='git show-branch --all'
  # alias gbv='git branch --verbose'
  # alias gbV='git branch --verbose --verbose'
  # alias gbx='git branch --delete'
  # alias gbX='git branch --delete --force'

  # # Commit (c)
  # alias gc='git commit --verbose'
  # alias gca='git commit --verbose --all'
  # alias gcm='git commit --message'
  # alias gcS='git commit -S --verbose'
  # alias gcSa='git commit -S --verbose --all'
  # alias gcSm='git commit -S --message'
  # alias gcam='git commit --all --message'
  # alias gco='git checkout'
  # alias gcO='git checkout --patch'
  # alias gcf='git commit --amend --reuse-message HEAD'
  # alias gcSf='git commit -S --amend --reuse-message HEAD'
  # alias gcF='git commit --verbose --amend'
  # alias gcSF='git commit -S --verbose --amend'
  # alias gcp='git cherry-pick --ff'
  # alias gcP='git cherry-pick --no-commit'
  # alias gcr='git revert'
  # alias gcR='git reset "HEAD^"'
  # alias gcs='git show'
  # alias gcl='git-commit-lost'
  # alias gcy='git cherry -v --abbrev'
  # alias gcY='git cherry -v'

  # Conflict (C)
  # alias gCl='git --no-pager diff --name-only --diff-filter=U'
  # alias gCa='git add (gCl)'
  # alias gCe='git mergetool (gCl)'
  # alias gCo='git checkout --ours --'
  # alias gCO='gCo (gCl)'
  # alias gCt='git checkout --theirs --'
  # alias gCT='gCt (gCl)'

  # Data (d)
  # alias gd='git ls-files'
  # alias gdc='git ls-files --cached'
  # alias gdx='git ls-files --deleted'
  # alias gdm='git ls-files --modified'
  # alias gdu='git ls-files --other --exclude-standard'
  # alias gdk='git ls-files --killed'
  # alias gdi='git status --porcelain --short --ignored | sed -n "s/^!! //p"'

  # Fetch (f)
  # alias gf='git fetch'
  # alias gfa='git fetch --all'
  # alias gfc='git clone'
  # alias gfcr='git clone --recurse-submodules'
  # alias gfm='git pull'
  # alias gfr='git pull --rebase'

  # Grep (g)
  # alias gg='git grep'
  # alias ggi='git grep --ignore-case'
  # alias ggl='git grep --files-with-matches'
  # alias ggL='git grep --files-without-matches'
  # alias ggv='git grep --invert-match'
  # alias ggw='git grep --word-regexp'

  # Index (i)
  # alias gia='git add'
  # alias giA='git add --patch'
  # alias giu='git add --update'
  # alias gid='git diff --no-ext-diff --cached'
  # alias giD='git diff --no-ext-diff --cached --word-diff'
  # alias gii='git update-index --assume-unchanged'
  # alias giI='git update-index --no-assume-unchanged'
  # alias gir='git reset'
  # alias giR='git reset --patch'
  # alias gix='git rm -r --cached'
  # alias giX='git rm -rf --cached'

  # Log (l)
  alias gl='git log --topo-order --pretty=format:"$_git_log_medium_format"'
  alias gls='git log --topo-order --stat --pretty=format:"$_git_log_medium_format"'
  alias gld='git log --topo-order --stat --patch --full-diff --pretty=format:"$_git_log_medium_format"'
  alias glo='git log --topo-order --pretty=format:"$_git_log_oneline_format"'
  alias glg='git log --topo-order --all --graph --pretty=format:"$_git_log_oneline_format"'
  alias glb='git log --topo-order --pretty=format:"$_git_log_brief_format"'
  alias glc='git shortlog --summary --numbered'

  # # Merge (m)
  # alias gm='git merge'
  # alias gmC='git merge --no-commit'
  # alias gmF='git merge --no-ff'
  # alias gma='git merge --abort'
  # alias gmt='git mergetool'

  # # Push (p)
  # alias gp='git push'
  # alias gpf='git push --force-with-lease'
  # alias gpF='git push --force'
  # alias gpa='git push --all'
  # alias gpA='git push --all; and git push --tags'
  # alias gpt='git push --tags'
  # alias gpc='git push --set-upstream origin "(git-branch-current 2> /dev/null)"'
  # alias gpp='git pull origin "(git-branch-current 2> /dev/null)"; and git push origin "(git-branch-current 2> /dev/null)"'

  # # Rebase (r)
  # alias gr='git rebase'
  # alias gra='git rebase --abort'
  # alias grc='git rebase --continue'
  # alias gri='git rebase --interactive'
  # alias grs='git rebase --skip'

  # # Remote (R)
  # alias gR='git remote'
  # alias gRl='git remote --verbose'
  # alias gRa='git remote add'
  # alias gRx='git remote rm'
  # alias gRm='git remote rename'
  # alias gRu='git remote update'
  # alias gRp='git remote prune'
  # alias gRs='git remote show'
  # alias gRb='git-hub-browse'

  # # Stash (s)
  # alias gs='git stash'
  # alias gsa='git stash apply'
  # alias gsx='git stash drop'
  # alias gsX='git-stash-clear-interactive'
  # alias gsl='git stash list'
  # alias gsL='git-stash-dropped'
  # alias gsd='git stash show --patch --stat'
  # alias gsp='git stash pop'
  # alias gsr='git-stash-recover'
  # alias gss='git stash save --include-untracked'
  # alias gsS='git stash save --patch --no-keep-index'
  # alias gsw='git stash save --include-untracked --keep-index'

  # # Submodule (S)
  # alias gS='git submodule'
  # alias gSa='git submodule add'
  # alias gSf='git submodule foreach'
  # alias gSi='git submodule init'
  # alias gSI='git submodule update --init --recursive'
  # alias gSl='git submodule status'
  # alias gSm='git-submodule-move'
  # alias gSs='git submodule sync'
  # alias gSu='git submodule foreach git pull origin master'
  # alias gSx='git-submodule-remove'

  # # Tag (t)
  # alias gt='git tag'
  # alias gtl='git tag -l'

  # # Working Copy (w)
  # alias gws='git status --ignore-submodules=$_git_status_ignore_submodules --short'
  # alias gwS='git status --ignore-submodules=$_git_status_ignore_submodules'
  # alias gwd='git diff --no-ext-diff'
  # alias gwD='git diff --no-ext-diff --word-diff'
  # alias gwr='git reset --soft'
  # alias gwR='git reset --hard'
  # alias gwc='git clean -n'
  # alias gwC='git clean -f'
  # alias gwx='git rm -r'
  # alias gwX='git rm -rf'
end


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/ichi/downloads/google-cloud-sdk/path.fish.inc' ]; . '/home/ichi/downloads/google-cloud-sdk/path.fish.inc'; end

