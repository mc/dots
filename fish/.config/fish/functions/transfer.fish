# Defined in /tmp/fish.9zKWe1/transfer.fish @ line 2
function transfer
	if test (count $argv) -ne 1
		echo "needs exactly one file argument."
		return 1
	end
	set tmpfile (mktemp -t transferXXX)
	curl --progress-bar --upload-file $argv https://transfer.sh/ >> $tmpfile
	cat $tmpfile
	rm -f $tmpfile
end
