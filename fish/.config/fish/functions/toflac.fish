# Defined in /tmp/fish.61H8IP/toflac.fish @ line 2
function toflac
	for f in $argv
    set rootname (string split -r -m1 . $f)[1]
    ffmpeg -v quiet -stats -i "$f" "$rootname.flac"
  end
end
