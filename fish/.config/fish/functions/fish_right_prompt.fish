function fish_right_prompt
  # don't do git status on sshfs mounts
  set fstype (findmnt -n -o FSTYPE -T "$PWD")
  if string match -q "fuse.*" "$fstype"
    echo (set_color yellow)"("(string split . "$fstype")[2]")"(set_color normal)" "
  else
    set branch (git rev-parse --abbrev-ref HEAD 2> /dev/null)
    if test $status -eq 0
      set modified_files (git ls-files --others --modified --exclude-standard)
      if string length -q $modified_files
        set dirty (set_color red --bold)"* "(set_color normal)
      end
      echo "$dirty"(set_color -d)"$branch "(set_color normal)
    end
  end
end

