function jewp -d "Send torrents to jewpiter"
    for torrent in $argv
        if test -f $torrent; and string match -q "*.torrent" "$torrent"
            set announce (lstor "$torrent" -qo announce) 
            set name (lstor "$torrent" -qo info.name) 
            switch $announce
            case '*landof.tv*'
                mv "$torrent" ~/mnt/jewpiter/downloads/watch/tv/
                echo -e "\033[0;32m tv    <- $name \033[0m"
            case '*passthepopcorn.me*'
                mv "$torrent" ~/mnt/jewpiter/downloads/watch/movies/
                echo -e "\033[0;32m movie <- $name \033[0m"
            case '*flacsfor.me*'
                mv "$torrent" ~/mnt/jewpiter/downloads/watch/music/
                echo -e "\033[0;32m music <- $name \033[0m"
            case '*'
                echo -e "\033[0;31m unrecognized tracker. \033[0m"
            end
        else
            echo -e "\033[0;31m no torrent file specified. \033[0m"
        end
    end
end

