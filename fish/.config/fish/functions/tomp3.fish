# Defined in /tmp/fish.zvf4EE/tomp3.fish @ line 2
function tomp3
	for f in $argv
    set rootname (string split -r -m1 . $f)[1]
    ffmpeg -v quiet -stats -i "$f" -c:a libmp3lame -q:a 0 "$rootname.mp3"
  end
end
