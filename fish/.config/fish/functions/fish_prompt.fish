function fish_prompt
  set last_status $status
  set_color blue

  set dirs (string replace "$HOME" "~" (pwd))

  if test $last_status -ne 0
    set triangle "△"
  else
    set triangle "▲"
  end

  echo -n " "(set_color red)"$triangle"(set_color blue)" $dirs "(set_color normal)
end
