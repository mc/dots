"" plugins

" install vim-plug if not already installed.
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-sensible'       " baseline
Plug 'bling/vim-airline'	    " statusbar
Plug 'justinmk/vim-sneak'       " s{char}{char} to jump to 2-gram, ';' key for next
Plug 'mhinz/vim-signify'        " git diff in gutter
Plug 'tpope/vim-fugitive'       " git wrapper
Plug 'tpope/vim-surround'       " surround block, ex. `ysiw'` to wrap word with '
Plug 'tpope/vim-sleuth'         " auto-detect indentation
Plug 'junegunn/vim-easy-align'  " alignment
Plug 'flazz/vim-colorschemes'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'digitaltoad/vim-pug'
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
Plug 'zah/nim.vim', { 'for': 'nim' }
Plug 'dag/vim-fish', { 'for': 'fish' }
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'cespare/vim-toml', { 'for': 'toml' }
Plug 'elzr/vim-json', { 'for': 'json' }
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
Plug 'junegunn/goyo.vim', { 'for': 'markdown' } " zen mode
Plug 'tikhomirov/vim-glsl'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'Firef0x/PKGBUILD.vim'

" Plug 'scrooloose/nerdtree'
" Plug 'scrooloose/nerdcommenter' " easy comments
" Plug 'kien/ctrlp.vim'           " fuzzy find files

call plug#end()

"" personal

" limit swapfile clutter

" for some reason the below lines make vim start *very slowly*
if !isdirectory($HOME . "/.vim/swapfiles")
    call mkdir($HOME . "/.vim/swapfiles", "p", 0700)
endif
if !isdirectory($HOME . "/.vim/backupfiles")
    call mkdir($HOME . "/.vim/backupfiles", "p", 0700)
endif
set directory=$HOME/.vim/swapfiles//
set backupdir=$HOME/.vim/backupfiles

" enable mouse input
set mouse=a

" disable word wrap
set nowrap

set ttyfast

" tab bullshit
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

" smart-case-sensitive searching
set ignorecase
set smartcase

" line number gutter
set number

" easy split window switching
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright

" easy tab navigation similar to vimium/qute
nnoremap H gT
nnoremap L gt

" yummy colors
" if !has('nvim')
"   set t_Co=256
"   colorscheme gruvbox
" endif
colorscheme wombat256mod

highlight LineNr ctermfg=DarkGrey guifg=DarkGrey

"" junegunn/vim-easy-align

xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

"" elzr/vim-json
" disable hiding double-quotes, etc.
let g:vim_json_syntax_conceal = 0

"" bling/vim-airline

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'jsformatter'
" let g:airline#extensions#tabline#left_sep = ' '
" let g:airline#extensions#tabline#left_alt_sep = '|'

"" plasticboy/vim-markdown

let g:vim_markdown_folding_disabled = 1

"" junegunn/fzf

nmap <C-p> :Files<cr>
imap <c-x><c-l> <plug>(fzf-complete-line)

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-i': 'split',
  \ 'ctrl-s': 'vsplit' }
let g:fzf_layout = { 'down': '~20%' }

let g:rg_command = '
  \ rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --color "always"
  \ -g "!{.config,.git,dist}/*" '

command! -bang -nargs=* F call fzf#vim#grep(g:rg_command .shellescape(<q-args>), 1, <bang>0)

"" mutt editing helper (limit text to 72 chars)
au BufRead /tmp/mutt-* set tw=72

"" LanguageClient-neovim

" Required for operations modifying multiple buffers like rename.
set hidden

let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'nightly', 'rls'],
    \ }

nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" Disable Background Color Erase for compatibility with kitty
" set t_ut=

" use ctrl-c to copy text to xclip
vnoremap <C-c> "+y

" don't clobber the buffer when pasting
vnoremap p "_dP

