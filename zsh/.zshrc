# ---- paths ----

PATH="$HOME/.local/bin:$PATH"
PATH="$HOME/go/bin:$PATH"
PATH="$HOME/bin:$PATH"
export EDITOR=vim
export GOPATH="$HOME/go"


# if [[ "$TERM" != (screen*) ]]; then
# 	random_retro_img
# fi
# cat ~/.local/etc/meow

# ---- debug ----

# zmodload zsh/zprof
# ^ uncomment to be able to call `zprof` and see zsh profiling stats


# ---- tools ----

export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow -g "!{.git,node_modules}/*" 2> /dev/null'
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# ---- antigen ----

# export NVM_LAZY_LOAD=true
source $HOME/antigen.zsh

antigen use oh-my-zsh
antigen bundle sorin-ionescu/prezto modules/git/alias.zsh

# antigen bundle lukechilds/zsh-nvm

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle zsh-users/zsh-autosuggestions

PROMPT_GEOMETRY_COLORIZE_SYMBOL=true
GEOMETRY_PROMPT_PLUGINS=(exec_time git rustup)
antigen theme geometry-zsh/geometry

antigen apply

# mount jewpiter if it's not mounted
if ! mount | grep sshfs | grep -q jewpiter; then
  sshfs jewpiter:/home/kaonashi /home/ichi/mnt/jewpiter \
    -o idmap=user \
    -o delay_connect \
    -o reconnect \
    -o ServerAliveInterval=15 \
    -o ServerAliveCountMax=3
  echo -e '\033[0;34m * jewpiter mounted.\033[0m'
fi

source $HOME/.zsh/alias.zsh
source $HOME/.zsh/functions.zsh

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/vault vault

source $HOME/.config/broot/launcher/bash/br

