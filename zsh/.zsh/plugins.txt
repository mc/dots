
robbyrussell/oh-my-zsh
antigen bundle sorin-ionescu/prezto folder:modules/git/

# antigen bundle lukechilds/zsh-nvm

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle zsh-users/zsh-autosuggestions

PROMPT_GEOMETRY_COLORIZE_SYMBOL=true
GEOMETRY_PROMPT_PLUGINS=(exec_time git rustup)
antigen theme geometry-zsh/geometry

antigen apply

source $HOME/.zsh/alias.zsh
source $HOME/.zsh/functions.zsh
