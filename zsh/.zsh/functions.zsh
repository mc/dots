# UGLY HACK to make nvm not take an extra couple of SECONDS starting zsh every time
# jesus fucking christ node and nvm what a shitshow
nvm() {
    unset -f nvm
    export NVM_DIR=~/.nvm
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
    nvm "$@"
}

node() {
    unset -f node
    export NVM_DIR=~/.nvm
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
    node "$@"
}

npm() {
    unset -f npm
    export NVM_DIR=~/.nvm
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
    npm "$@"
}

function wifi-password() {
    # ssid=$(nmcli -t -f active,ssid dev wifi | sed -rn 's/^yes:(.*)/\1/p')
    ssid=$(iwgetid -r)
    if [[ "$?" -ne "0" ]]; then
	echo -e "\033[0;31m not connected to wifi. \033[0m"
    fi
    password=$(sudo cat "/etc/NetworkManager/system-connections/$ssid" | sed -rn 's/^psk=(.*)/\1/p')
    echo -e "\033[1;32m ${password} \033[0m"
}

# edit zshrc, create a backup, and reload if changes were made
function ae() {
	vim -p $HOME/.zsh/*.zsh
	echo -e '\033[0;34m * reloading aliases...\033[0m'
	source $HOME/.zsh/*.zsh
}

function jewp() {
    if [[ ( -f $1 ) && ( $1 == *.torrent ) ]]; then
	local announce=$(lstor "$1" -qo announce)
	case $announce in
	    *landof.tv*)         cp "$1" ~/mnt/jewpiter/downloads/watch/tv/;     echo -e "\033[0;32m jewped tv. \033[0m"    ;;
	    *passthepopcorn.me*) cp "$1" ~/mnt/jewpiter/downloads/watch/movies/; echo -e "\033[0;32m jewped movie. \033[0m" ;;
	    *flacsfor.me*)       cp "$1" ~/mnt/jewpiter/downloads/watch/music/;  echo -e "\033[0;32m jewped music. \033[0m" ;;
	    *)			 echo -e "\033[0;31m unrecognized tracker. \033[0m"
	esac
    else
	echo -e "\033[0;31m no torrent file specified. \033[0m"
    fi
}

function weather() {
	url="https://wttr.in"
	if (($# != 0)) then
		url="$url/$@"
	fi
	curl -s --insecure "$url" | head -7
}

function forecast() {
	url="https://wttr.in"
	if (($# != 0)) then
		url="$url/$@"
	fi
	curl -s --insecure "$url" | head -37 | tail -30
}


# quick convert any 
function tomp3() {
	if (( $# == 0 )); then
		echo "usage: tomp3 input"
	fi
	ffmpeg -v quiet -stats -i "$1" -c:a libmp3lame -q:a 0 "${1%.*}.mp3"
}
#
# quick convert any 
function toflac() {
	if (( $# == 0 )); then
		echo "usage: tomp3 input"
	fi
	ffmpeg -v quiet -stats -i "$1" "${1%.*}.flac"
}

function toprores() {
	if (( $# == 0 )); then
		echo "usage: toprores input"
	fi
	ffmpeg -v quiet -stats -i "$1" -c:v prores -profile:v 3 "${1%.*}.mov"
}

function v() {
    if [[ $# -lt 2 ]]; then
	echo "usage; v [vmname] [command]"
    fi
    cd "$HOME/vagrant/$1" && vagrant ${@:2}; cd -
}

function colortest_basic() {
	for attr in $(seq 0 1); do
		for fg in $(seq 30 37); do
			for bg in $(seq 40 47); do
				printf "\033[$attr;${bg};${fg}m$attr;$fg;$bg\033[m "
			done
			printf "\n"
		done
	done
}

function colortest_256() {
	print_256colors
}

# modified from the example at transfer.sh
function transfer() {
	if [ $# -eq 0 ]; then
		echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"
		return 1
	fi 
	tmpfile=$( mktemp -t transferXXX )
	if tty -s; then
		basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g')
		curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile
	else
		curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile
	fi; 
	cat $tmpfile
	rm -f $tmpfile
} 

man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}
