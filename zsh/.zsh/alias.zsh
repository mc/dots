# ---- aliases ----

# alias vim="nvim"
# alias vi="nvim"

alias df='df -h'
alias ls='exa'
alias ll='exa -alh -snew --group-directories-first'
alias lf="du -sh {.,}* | gsort -h"
alias lsnow="ll *(mm-5)"
alias lstoday="ll *(mh-24)"
alias bu="brew update && brew upgrade && brew cleanup"
alias bi="brew install"
alias bs="brew search"
alias x="exa --group-directories-first"
alias zr="source $HOME/.zshrc"
alias R="rsync -shavP"
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'
alias porn=' mpv "http://www.pornhub.com/random"'
#alias cls=' clear; tmux clear-history'
alias cls="echo -e '\0033\0143'"
alias lock='$HOME/.config/i3/lock.sh'
alias myip='curl icanhazip.com'
alias prettyping='prettyping --nolegend'
alias ytdl='youtube-dl --output ~/Videos/'
alias ytdlmq='youtube-dl --output ~/Videos/ -f "bestvideo[height<=720]+bestaudio/best[height<=720]"'

alias open='xdg-open'
alias :q='exit'
alias dud='du -d 1 -h'
alias duf='du -sh *'
alias p='ps axo pid,user,pcpu,comm'
alias mp3-dl='youtube-dl --ignore-config --extract-audio \
    --audio-format "mp3" --audio-quality 0 --embed-thumbnail \
    --add-metadata --metadata-from-title "%(artist)s - %(title)s" \
    --output "%(title)s.%(ext)s"'

alias caffeinate='systemd-inhibit --what=handle-lid-switch --why="caffeinated"'
alias myip='dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | awk -F'\''"'\'' '\''{ print $2}'\'''

